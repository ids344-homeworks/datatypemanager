// IDS344 - 2022-01 - Grupo 1 - QueueManagement
//
// Nikita Kravchenko - 1101607
// Omar N��ez - 1101587
// Oliver Infante - 1100292
// Lervis Pinales - 1096323
// Daniel B�ez - 1073597
#include <iostream>

using namespace std;

struct QueueNode;
struct Queue;

struct QueueNode* CreateQueueNode(string data, int priority);
struct Queue* CreateQueue();

void Enqueue(Queue*& queue, string data, int priority);
void EnqueueValue(Queue*& queue, string text, int priority);
std::string Dequeue(Queue*& queue);
bool IsQueueEmpty(Queue*& queue);
void SortByPriority(Queue*& queue);
void ShowAllQueue(Queue*& queue);
int ModifyPriority(int prior);