// IDS344 - 2022-01 - Grupo 1 - DataTypeManagement
// 
// Nikita Kravchenko - 1101607
// Omar N��ez - 1101587
// Oliver Infante - 1100292
// Lervis Pinales - 1096323
// Daniel B�ez - 1073597

#include <iostream>
#include <Windows.h>
#include <string>
#include <iostream>

#include "GenericFunctions.h"
#include "StackManager.h"
#include "QueueManager.h"
#include "ListaEnlazadaManager.h"

#include "DataTypeManager.h"

using namespace std;

const string programName = "Data Type Manager";

int main()
{
    while (true)
    {
        DisplayMainMenu();
    }
}

void DisplayMainMenu()
{
    string menuOptionString;
    int menuOption = 0;

    ClearConsole(programName);

    cout << "Select an option. (1 - 3)\n";
    cout << "1. Stack manager\n";
    cout << "2. Queue manager\n";
    cout << "3. Lista enlazada\n";
    cout << "...Coming soon...\n";
    cout << "3. Exit\n\n";
    cout << ">> ";

    cin >> menuOptionString;

    menuOption = IsNumber(menuOptionString) ? stoi(menuOptionString) : 0;

    //if (IsNumber(menuOptionString)) menuOption = stoi(menuOptionString); //stoi to check if its number
    //else menuOption = 0;

    switch (menuOption)
    {
        case 1:
            StartStackManager(); 
            break;
        case 2:
            StartQueueManager();
            break;
        case 3:
            ExitProgram();
            break;
        default:
            cout << "Please, enter a valid option. (1 - 3)\n";
            Pause();
            break;
    }

    ClearConsole(programName);
}
