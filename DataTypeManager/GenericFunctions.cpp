

#include <iostream>
#include <string> //stoi to check if its number

using namespace std;

// Checks if string is a number
bool IsNumber(string s)
{
    for (int i = 0; i < s.length(); i++)
        if (isdigit(s[i]) == false)
            return false;

    return true;
}

// Closes program.
void ExitProgram()
{
    exit(EXIT_SUCCESS);
}

// Closes program.
void ClearConsole(string textToShow)
{
    system("cls");
    cout << "-- " << textToShow << " --\n\n";
}


//Waits for a user to press some button
void Pause()
{
    cout << "\n";
    system("pause");
}

