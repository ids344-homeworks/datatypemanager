// IDS344 - 2022-01 - Grupo 1 - QueueManagement
//
// Nikita Kravchenko - 1101607
// Omar N��ez - 1101587
// Oliver Infante - 1100292
// Lervis Pinales - 1096323
// Daniel B�ez - 1073597

#include <iostream>
#include "StackStruct.h" 

using namespace std;

struct StackNode
{
    string data;
    StackNode* next = NULL;
};

struct StackNode* CreateStackNode(string data)
{
    struct StackNode * newNode = new (struct StackNode);
    
    newNode->data = data;
    newNode->next = NULL;

    return newNode;

    printf("sdfa");
}

// se agrega un StackNode
void Push(StackNode*& stack, string data)
{
    StackNode* newNode = CreateStackNode(data);

    newNode->next = stack;

    stack = newNode;
}

// se elimina un StackNode
string Pop(StackNode*& stack)
{
    StackNode* aux = stack;
    string data = "";

    if (aux != NULL) {
        data = aux->data;
        stack = aux->next;

        delete aux;
    }
    return data;
}


// vaciar el stack
bool IsStackEmpty(StackNode*& stack)
{
    bool answer = false;

    if (stack == NULL) answer = true;
    return answer;
}

//// ayuda a aprender la vinculacion de StackNodes por puntero
//void DebugMemory(StackNode* stack)
//{
//    cout << *&stack << " is *&; " << &stack << " is &; " << stack << " is stack" << endl;
//    cout << *&stack->data << " is a data *&; " << &stack->data << " is data &; " << stack->data << " is data" << endl;
//    cout << *&stack->next << " is next *&; " << &stack->next << " is next &; " << stack->next << " is next \n" << endl;
//}
