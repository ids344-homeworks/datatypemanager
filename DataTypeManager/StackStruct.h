#include <iostream>
using namespace std;

struct StackNode;
struct StackNode* CreateStackNode(std::string data);

void Push(StackNode*& stack, string data);
std::string Pop(StackNode*& stack);
bool IsStackEmpty(StackNode*& stack);
//void DebugMemory(StackNode* stack);
